<?php
/**
 * Plugin Name: Oksana Calendar
 * Description: Календарь мероприятий для сайты Оксаны Басалай
 * Author: Web - Iden
 * Version: 0.1
 */

define( 'OKSANA_CALENDAR_VALUES_DIR', plugin_dir_path( __FILE__ ) );
define( 'OKSANA_CALENDAR_VALUES_URL', plugin_dir_url( __FILE__ ) );

include OKSANA_CALENDAR_VALUES_DIR . '/include/Custom_types.php';
include OKSANA_CALENDAR_VALUES_DIR . '/include/Settings.php';

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wpb_widget' );
}

add_action( 'widgets_init', 'wpb_load_widget' );

// Creating the widget
class wpb_widget extends WP_Widget {

	function __construct() {
		parent::__construct( 'wpb_widget', __( 'Oksana Calendar', 'wpb_widget_domain' ), array( 'description' => __( 'Oksana Calendar', 'wpb_widget_domain' ), ) );
	}

	/**
	 * @return array
	 */
	private function getStyleFromOptions() {
		$td_background_color        = get_option( 'td_background_color' );//Цвет ячеек таблицы
		$td_active_background_color = get_option( 'td_active_background_color' );//Цвет активных ячеек

		$td_a_color = get_option( 'td_a_color' );//Цвет ссылок

		$td_holidays_color            = get_option( 'td_holidays_color' );//Цвет текста выходных
		$td_holidays_background_color = get_option( 'td_holidays_background_color' );//Цвет фона выходных

		//Названия дней недели
		$td_days_names_background    = get_option( 'td_days_names_background' );//Задний фон дней недели
		$td_days_names_color         = get_option( 'td_days_names_color' );//Цвет текста рабочих дней недели
		$td_holiday_days_names_color = get_option( 'td_holiday_days_names_color' );//Цвет текста выходных


		$styles = '';

		if ( $td_background_color ) {
			$styles .= '.day { background-color: #'.$td_background_color.';}';
		}

		if ( $td_active_background_color ) {
			$styles .= '.active-day { background-color: #'.$td_active_background_color.'!important; }';
		}

		if ( $td_a_color ) {
			$styles .= '.active-day a { color: #'.$td_a_color.'!important;}';
		}

		if ( $td_holidays_color  ) {
			$styles .= '.calendar-dow td:nth-child(n+6) .day { color: #'.$td_holidays_color.'!important;}';
		}

		if ( $td_holidays_background_color  ) {
			$styles .= '.calendar-dow td:nth-child(n+6) .day { background-color: #'.$td_holidays_background_color.';}';
		}

		if ($td_days_names_background) {
		    $styles .= '.calendar-dow-header th {background-color:#'.$td_days_names_background.';}';
        }

		if ($td_days_names_color) {
			$styles .= '.calendar-dow-header th {color:#'.$td_days_names_color.';}';
		}

		if ($td_holiday_days_names_color) {
			$styles .= '.calendar-dow-header th:nth-child(n+6) {color:#'.$td_holiday_days_names_color.';}';
		}

		return $styles;
	}

	/**
	 * Получаем события >= определенной даты
	 *
	 * @param $today
	 *
	 * @return array
	 */
	private function getEvents( $today ) {
		$argrs = array(
			'post_type'      => 'events',
			'meta_key'       => 'event_sysdate',
			'meta_query'     => array(
				array(
					'key'     => 'event_sysdate',
					'value'   => $today,
					'compare' => '>=',
					'type' => 'DATE'
				)
			),
			'orderby'        => 'meta_value',
			'order'          => 'ASC',
			'posts_per_page' => -1
		);

		return get_posts( $argrs );
	}

	/**
	 * Добавляю JS файл календаря
	 */
	private function initJS() {
		wp_enqueue_script( 'oks-zabuto-calendar', OKSANA_CALENDAR_VALUES_URL . 'plugins/zabuto-calendar/zabuto_calendar.js' );
		wp_enqueue_style( 'oks-zabuto-calendar', OKSANA_CALENDAR_VALUES_URL . 'plugins/zabuto-calendar/zabuto_calendar.css' );
	}

	/**
     * Создает массив с эвентами в JSON
	 * @param $events
	 *
	 * @return false|string
	 */
	private function getJson($events) {
		$json = [];
	    foreach ( $events as $event ) {
		    $event_date = get_post_meta( $event->ID, 'event_sysdate', true );
		    $event_link = get_post_meta( $event->ID, 'svgiconcode', true );

		    if (($timestamp = strtotime($event_date)) !== false) {
			   $format_date = date('Y-m-d', $timestamp);
			    $json[] = ['title' => $event->post_title, 'date' => $format_date, 'link' => $event_link, "badge" => true, 'classname' => 'active-day' ];
		    }

	    }

	    return wp_json_encode($json,JSON_UNESCAPED_UNICODE);
    }

	public function widget( $args, $instance ) {

		add_action('wp_enqueue_scripts', $this->initJS());

		$styles = $this->getStyleFromOptions();
		$today = date( 'Y/m/d' );
		$events = $this->getEvents( $today );
		if($styles) {
		    echo '<style>'.$styles.'</style>';
        }
        $json = $this->getJson($events);
		// Выводим Стили
		echo '<style>';
		if ( $styles['td_days_names_background'] ) {
			echo '.day-names th { ' . $styles['td_days_names_background'] . '}';
		}
		if ( $styles['td_days_names_color'] ) {
			echo '.day-names th { ' . $styles['td_days_names_color'] . '}';
		}

		if ( $styles['td_holiday_days_names_color'] ) {
			echo '.day-names .holiday { ' . $styles['td_holiday_days_names_color'] . '}';
		}

		echo '</style>';
		echo '<div class="widget">';
		$title = apply_filters( 'widget_title', $instance['title'] );
		if ( ! empty( $title ) ) {
			echo '<h2>' . $args['before_title'] . $title . $args['after_title'] . '</h2>';
		}
		echo '<div id="oks-calendar-1"></div>';
		echo '</div>';
		echo "<script>
                jQuery(document).ready(function() {
                    var eventData = ".$json.";
    
                   jQuery('#oks-calendar-1').zabuto_calendar({
                       data: eventData,
                       language: 'ru'
                   });
                });
           
            </script>";

	}

// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}
// Widget admin form
		?>


        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
               name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
               value="<?php echo esc_attr( $title ); ?>"/>


		<?php
	}

// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance          = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}
} // Class wpb_widget ends here