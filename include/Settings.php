<?php

add_action( 'admin_menu', 'register_my_custom_submenu_page' );

function register_my_custom_submenu_page() {
	add_submenu_page( 'tools.php', 'Каледарь для Оксаны', 'Каледарь Оксаны', 'manage_options', 'my-custom-submenu-page', 'my_custom_submenu_page_callback' );
}

//call register settings function
add_action( 'admin_init', 'register_mysettings' );
/**
 * Регистрируем настройки плагина
 */
function register_mysettings() {
	//register our settings
	register_setting( 'oks-settings-group', 'td_background_color' );
	register_setting( 'oks-settings-group', 'td_active_background_color' );
	register_setting( 'oks-settings-group', 'td_a_color' );
	register_setting( 'oks-settings-group', 'td_holidays_color' );
	register_setting( 'oks-settings-group', 'td_holidays_background_color' );

	register_setting( 'oks-settings-group', 'td_holiday_days_names_color' );
	register_setting( 'oks-settings-group', 'td_days_names_color' );
	register_setting( 'oks-settings-group', 'td_days_names_background' );
}

function my_custom_submenu_page_callback() {
	echo '<link rel="stylesheet" media="screen" type="text/css" href="' . OKSANA_CALENDAR_VALUES_URL . '/plugins/colorpicker/colorpicker.css" />';
	echo '<script type="text/javascript" src="' . OKSANA_CALENDAR_VALUES_URL . '/plugins/colorpicker/colorpicker.js"></script>';

	// контент страницы
	echo '<div class="wrap">';
	echo '<h2>' . get_admin_page_title() . '</h2>';
	echo '</div>'; ?>
    <div class="wrap">
        <form method="post" action="options.php">
			<?php settings_fields( 'oks-settings-group' ); ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Цвет фона ячеек календаря</th>
                    <td>
                        <input type="text" placeholder="Цвет фона ячеек календаря" id="td_background_color"
                               name="td_background_color" value="<?php echo get_option( 'td_background_color' ); ?>">
                        <div style="width: 20px; height: 20px; float: left; margin: 3px; background-color: #<?= get_option( 'td_background_color' ); ?>"
                             id="td_background_color_div"></div>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">Цвет фона ячеек выходных</th>
                    <td>
                        <input type="text" placeholder="Цвет фона ячеек выходных" id="td_holidays_background_color"
                               name="td_holidays_background_color"
                               value="<?php echo get_option( 'td_holidays_background_color' ); ?>">
                        <div style="width: 20px; height: 20px; float: left; margin: 3px; background-color: #<?= get_option( 'td_holidays_background_color' ); ?>"
                             id="td_holidays_background_color_div"></div>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Цвет фона ячеек со ссылками</th>
                    <td>
                        <input type="text" placeholder="Цвет фона ячеек со ссылками" id="td_active_background_color"
                               name="td_active_background_color"
                               value="<?php echo get_option( 'td_active_background_color' ); ?>">
                        <div style="width: 20px; height: 20px; float: left; margin: 3px; background-color: #<?= get_option( 'td_active_background_color' ); ?>"
                             id="td_active_background_color_div"></div>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">Цвет текста ссылок</th>
                    <td>
                        <input type="text" placeholder="Цвет текста ссылок" id="td_a_color" name="td_a_color"
                               value="<?php echo get_option( 'td_a_color' ); ?>">
                        <div style="width: 20px; height: 20px; float: left; margin: 3px; background-color: #<?= get_option( 'td_a_color' ); ?>"
                             id="td_a_color_div"></div>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">Цвет текста выходных дней</th>
                    <td>
                        <input type="text" placeholder="Цвет текста выходных дней" id="td_holidays_color"
                               name="td_holidays_color" value="<?php echo get_option( 'td_holidays_color' ); ?>">
                        <div style="width: 20px; height: 20px; float: left; margin: 3px; background-color: #<?= get_option( 'td_holidays_color' ); ?>"
                             id="td_holidays_color_div"></div>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">Цвет фона названий дней недели</th>
                    <td>
                        <input type="text" placeholder="Цвет фона названий дней недели" id="td_days_names_background"
                               name="td_days_names_background" value="<?php echo get_option( 'td_days_names_background' ); ?>">
                        <div style="width: 20px; height: 20px; float: left; margin: 3px; background-color: #<?= get_option( 'td_days_names_background' ); ?>"
                             id="td_days_names_background_div"></div>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">Цвет текста названий РАБОЧИХ дней недели</th>
                    <td>
                        <input type="text" placeholder="Цвет текста названий РАБОЧИХ дней недели" id="td_days_names_color"
                               name="td_days_names_color" value="<?php echo get_option( 'td_days_names_color' ); ?>">
                        <div style="width: 20px; height: 20px; float: left; margin: 3px; background-color: #<?= get_option( 'td_days_names_color' ); ?>"
                             id="td_days_names_color_div"></div>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">Цвет текста названий ВЫХОДНЫХ дней недели</th>
                    <td>
                        <input type="text" placeholder="Цвет текста названий ВЫХОДНЫХ дней недели" id="td_holiday_days_names_color"
                               name="td_holiday_days_names_color" value="<?php echo get_option( 'td_holiday_days_names_color' ); ?>">
                        <div style="width: 20px; height: 20px; float: left; margin: 3px; background-color: #<?= get_option( 'td_holiday_days_names_color' ); ?>"
                             id="td_holiday_days_names_color_div"></div>
                    </td>
                </tr>
            </table>

            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e( 'Save Changes' ) ?>"/>
            </p>

        </form>
    </div>

    <script>
        jQuery('#td_holidays_color, #td_a_color, #td_active_background_color, #td_holidays_background_color, #td_background_color, #td_holiday_days_names_color, ' +
            '#td_days_names_color, #td_days_names_background').ColorPicker({
            onSubmit: function (hsb, hex, rgb, el) {
                var div_name = '#' + jQuery(el).attr('id') + '_div';
                jQuery(div_name).css('background-color', '#' + hex);
                jQuery(el).val(hex);
                jQuery(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                jQuery(this).ColorPickerSetColor(this.value);
            }
        })
            .bind('keyup', function () {
                jQuery(this).ColorPickerSetColor(this.value);
            });
    </script>
<?php }