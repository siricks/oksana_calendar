<?php
//Добавление сущности мероприятие
add_action('init', 'project_init');
function project_init(){
	register_post_type('events', array(
		'labels'             => array(
			'name'               => 'Мероприятие', // Основное название типа записи
			'singular_name'      => 'Мероприятие', // отдельное название записи типа events
			'add_new'            => 'Добавить новое',
			'add_new_item'       => 'Добавить новое мероприятие',
			'edit_item'          => 'Редактировать мероприятие',
			'new_item'           => 'Новое мероприятие',
			'view_item'          => 'Посмотреть мероприятие',
			'search_items'       => 'Найти мероприятие',
			'not_found'          => 'Мероприятий не найдено',
			'not_found_in_trash' => 'В корзине мероприятий не найдено',
			'parent_item_colon'  => '',
			'menu_name'          => 'Мероприятия'

		),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 4,
		'supports'           => array('title'),
		'taxonomies'         => array(),
		'menu_icon'          => 'dashicons-admin-tools',
	) );
}

// Сообщения при публикации или изменении типа записи Мероприятие
add_filter('post_updated_messages', 'events_updated_messages');
function events_updated_messages( $messages ) {
	global $post;

	$messages['events'] = array(
		0 => '', // Не используется. Сообщения используются с индекса 1.
		1 => sprintf( 'Мероприятие обновлено. <a href="%s">Посмотреть запись типа мероприятие</a>', esc_url( get_permalink($post->ID) ) ),
		2 => 'Произвольное поле обновлено.',
		3 => 'Произвольное поле удалено.',
		4 => 'Запись мероприятия обновлена.',
		/* %s: дата и время ревизии */
		5 => isset($_GET['revision']) ? sprintf( 'Запись мероприятия восстановлена из ревизии %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( 'Запись мероприятия опубликована. <a href="%s">Перейти к записи мероприятия</a>', esc_url( get_permalink($post->ID) ) ),
		7 => 'Запись Мероприятиеа сохранена.',
		8 => sprintf( 'Запись сохранена. <a target="_blank" href="%s">Предпросмотр записи</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post->ID) ) ) ),
		9 => sprintf( 'Запись запланирована на: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Предпросмотр записи</a>',
			// Как форматировать даты в PHP можно посмотреть тут: http://php.net/date
			date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post->ID) ) ),
		10 => sprintf( 'Черновик записи Мероприятиеа обновлен. <a target="_blank" href="%s">Предпросмотр записи</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post->ID) ) ) ),
	);

	return $messages;
}

// Раздел "помощь" типа записи Мероприятие
add_action( 'contextual_help', 'add_help_text', 10, 3 );
function add_help_text( $contextual_help, $screen_id, $screen ){
	//$contextual_help .= print_r($screen); // используйте чтобы помочь определить параметр $screen->id
	if('events' == $screen->id ) {
		$contextual_help = '
		<p>Напоминалка при редактировании записи мероприятие</p>

		';
	}
	elseif( 'edit-events' == $screen->id ) {
		$contextual_help = '<p>Это раздел помощи показанный для типа записи мероприятие и т.д. и т.п.</p>';
	}

	return $contextual_help;
}


/*
 *	End Post MetaBoxes
 */
/*
*	Start Serveice MetaBox
*/
$servicesvg_option_meta = array(
	'id' => 'section-page-meta',
	'title' => __('Events Setting','andybar'),
	'page' => 'events',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
		array(
			'name' => __('Events link','andybar'),
			'desc' => __('Enter link here','andybar'),
			'id' => $prefix.'svgiconcode',
			'type' => 'textarea',
			'std' => ''
		),
		array(
			'name' =>  __('Event Date', 'andybar'),
			'desc' => '',
			'id' => $prefix.'event_sysdate',
			'type' => 'date',
			'std' => ''
		),
	)

);
// create boxes for team
add_action('admin_menu', 'add_servicesvg_meta_boxes');
if(!function_exists('add_servicesvg_meta_boxes')){
	function add_servicesvg_meta_boxes()
	{
		global  $servicesvg_option_meta;
		add_meta_box($servicesvg_option_meta['id'], $servicesvg_option_meta['title'], 'show_servicesvg_options', $servicesvg_option_meta['page'], $servicesvg_option_meta['context'], $servicesvg_option_meta['priority']);
	}
}
if(!function_exists('show_servicesvg_options')){
	function show_servicesvg_options()
	{
		global $servicesvg_option_meta, $post;
		// Use nonce for verification
		echo '<input type="hidden" name="meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
		echo '<table class="form-table">';
		foreach ($servicesvg_option_meta['fields'] as $field) {
			// get current post meta data
			$meta = get_post_meta($post->ID, $field['id'], true);
			switch ($field['type']) {
				case 'textarea':
					echo '<tr>',
					'<th style="width:25%"><label for="', $field['id'], '"><strong>', $field['name'], '</strong><span style=" display:block; color:#999; margin:5px 0 0 0; line-height: 18px;">' . $field['desc'] . '</span></label></th>',
					'<td>';
					echo '<textarea rows="20" cols="80" name="', $field['id'], '" id="', $field['id'], '" >', $meta ? $meta
						: stripslashes(htmlspecialchars(($field['std']), ENT_QUOTES)), '</textarea>';
					break;
				case 'date':
					echo '<tr>',
					'<th style="width:25%"><label for="', $field['id'], '"><strong>', $field['name'], '</strong><span style=" display:block; color:#999; margin:5px 0 0 0; line-height: 18px;">' . $field['desc'] . '</span></label></th>',
					'<td>';
					echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta
						: stripslashes(htmlspecialchars(($field['std']), ENT_QUOTES)), '" size="30" style="width:75%; margin-right: 20px; float:left;" class="andybar_event_date" />';
					break;
			}
		}
		echo '</table>';
	}
}


add_action('save_post', 'section_servicesvg_options');
// save metadata
if(!function_exists('section_servicesvg_options')){
	function section_servicesvg_options($post_id)
	{
		global $servicesvg_option_meta,$post;
		$new = '';
		// verify nonce
		if (isset($_POST['meta_box_nonce']) && !wp_verify_nonce($_POST['meta_box_nonce'], basename(__FILE__))) {
			return $post_id;
		}
		// check autosave
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post_id;
		}
		if (defined('DOING_AJAX') && DOING_AJAX)
			return;
		// check permissions
		if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {
			if (!current_user_can('edit_page', $post_id)) {
				return $post_id;
			}
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
		}
		foreach ($servicesvg_option_meta['fields'] as $field) {
			$old = get_post_meta($post_id, $field['id'], true);
			if (isset($_POST[$field['id']])) {
				$new = $_POST[$field['id']];
				if (($timestamp = strtotime($new)) !== false) {
					$new = date('Y/m/d', $timestamp);
				}
			}
			if ($new && $new != $old) {
				update_post_meta($post_id, $field['id'], $new);

			} elseif ('' == $new && $old) {
				delete_post_meta($post_id, $field['id'], $old);
			}
		}
	}
}