<?php
//Удаление настроек плагина
unregister_setting( 'oks-settings-group', 'td_background_color' );
unregister_setting( 'oks-settings-group', 'td_active_background_color' );
unregister_setting( 'oks-settings-group', 'td_a_color' );
unregister_setting( 'oks-settings-group', 'td_holidays_color' );
unregister_setting( 'oks-settings-group', 'td_holidays_background_color' );

unregister_setting( 'oks-settings-group', 'td_holiday_days_names_color' );
unregister_setting( 'oks-settings-group', 'td_days_names_color' );
unregister_setting( 'oks-settings-group', 'td_days_names_background' );